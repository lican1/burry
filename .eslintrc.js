/*
 * @Author: 李灿
 * @Date: 2021-11-30 15:46:10
 * @Description:
 */
const path = require("path")
const resolve = _path => path.resolve(__dirname, _path)

module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  files: ['*.ts', '*.tsx'], // Your TypeScript files extension
  parser: "@typescript-eslint/parser", // 配置ts解析器
  parserOptions: {
    project: resolve("./tsconfig.json"),
    tsconfigRootDir: resolve("./"),
    sourceType: "module",
  },
}
