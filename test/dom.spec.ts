/*
 * @Author: 李灿
 * @Date: 2021-12-22 11:16:04
 * @Description: 软件盘展开收起 case
 * @FilePath: /burries/test/dom.spec.ts
 * 
 * 
 * 在 window 上监听事件resize 事件，修改了 window.height 之后还，
 * 
 * 需要手动触发 resize: window.dispatchEvent(new Event('resize'));
 */
import { windowResize, } from "../src/index"
import assert from "power-assert"

describe('DOM API:', function () {
    jest.setTimeout(20000)

    describe('windowResize 监听h5 软件盘展开收起', function () {
        let isUp: boolean = false
        let isDown: boolean = true
        let innerHeight = window.innerHeight
        beforeAll(function () {
            const upCb = () => {
                isUp = true
                isDown = false
            }
            const downCb = () => {
                isUp = false
                isDown = true
            }

            windowResize(downCb, upCb)

        })
        test(`测试软件盘弹出`, function () {
            // 设置innerHeight，模拟软键盘弹起
            window.innerHeight = innerHeight - 200
            // 触发resize事件
            window.dispatchEvent(new Event('resize'));
            assert(isUp === true)
            assert(isDown === false)
        });


        test(`测试软件盘收起`, function () {
            // 设置innerHeight，模拟软键盘弹起
            window.innerHeight = innerHeight
            // 触发resize事件
            window.dispatchEvent(new Event('resize'));
            assert(isUp === false)
            assert(isDown === true)
        });

        afterAll(function () {
            window.innerHeight = innerHeight
        })
    });





})