/*
 * @Author: 李灿
 * @Date: 2021-11-30 14:21:51
 * @Description:数组是否相等测试案例
 * 1. 单元测试覆盖率
 */
import arrayEqual from "../src/array/arrayEqual"
import assert from "power-assert"
describe("Array API:", function () {
  // 长度不相等
  test(`arrayEqual([0, 2, 3, 4], [1, 2, 3]) should return false`, function () {
    assert.notEqual(arrayEqual([0, 2, 3, 4], [1, 2, 3]), true)
  })

  // 完全相同的数组

  test(`arrayEqual: array = array2, should return true`, function () {
    const array: Array<number> = [1, 2, 3]
    const array2 = array
    assert.equal(arrayEqual(array, array2), true)
  })

  // 相同长度，但元素不相等

  test(`arrayEqual([0, 2, 3], [1, 2, 3]) should return false`, function () {
    assert.equal(arrayEqual([0, 2, 3], [1, 2, 3]), false)
  })

  // 不同指针，但是元素相等
  test(`arrayEqual([1, 2, 3], [1, 2, 3]) should return true`, function () {
    assert.equal(arrayEqual([1, 2, 3], [1, 2, 3]), true)
  })
})
