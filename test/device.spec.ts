/*
 * @Author: 李灿
 * @Date: 2021-11-30 14:21:51
 * @Description:媒体查询测试案例，由于环境难以切换，直接代入字符串参数，
 * 
 * 1. 测试当前浏览器环境，测试框架不支持直接切换测试环境(jest 选的是 jsdom)。抽象为直接代入字符串校验
 */
import { getExplore, getOS } from "../src/index"
import assert from "power-assert"
interface INavigator {
  userAgent: string
  [prop: string]: any
}
interface IWindow {
  navigator: INavigator
}
// 浏览器 useragent
const chrome = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.55 Safari/537.36"
const IE = "User-Agent:Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0"
const IE2 = "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) like Gecko"
const firefox = "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1"
const opera = "User-Agent:Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; en) Presto/2.8.131 Version/11.11"
const safari = "User-Agent:Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50"
const edge = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36Edge/13.10586"

describe("device API: 判断浏览器类型", function () {
  test(`getExplore() Chrome`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: chrome
      }
    }
    assert(getExplore(shadowWindow).browser === 'Chrome')
  })

  test(`getExplore() IE`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: IE2
      }
    }
    assert(getExplore(shadowWindow).browser === "IE")
  })

  test(`getExplore() IE`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: IE
      }
    }
    assert(getExplore(shadowWindow).browser === "IE")
  })

  test(`getExplore() Firefox`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: firefox
      }
    }
    assert(getExplore(shadowWindow).browser === "Firefox")
  })

  test(`getExplore() Opera`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: opera
      }
    }
    assert(getExplore(shadowWindow).browser === "Opera")
  })

  test(`getExplore() Safari`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: safari
      }
    }
    assert(getExplore(shadowWindow).browser === "Safari")
  })
  test(`getExplore() Edge`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: edge
      }
    }
    assert(getExplore(shadowWindow).browser === "EDGE")
  })
  test(`getExplore() Jsdom`, function () {
    assert(getExplore().browser === "not fund")
  })


})


// 操作系统类型
const windowsPhone = "Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0; HTC; Titan)"
const ios = "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5"
const android = "MQQBrowser/26 Mozilla/5.0 (linux; U; Android 2.3.7; zh-cn; MB200 Build/GRJ22; CyanogenMod-7) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"
const macOs = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_8; en-us) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50"
const windows = "User-Agent:Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0;"
const linux = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:21.0) Gecko/20130331 Firefox/21.0"

describe("device API: 判断操作系统类型", function () {
  test(`getOS() windowsPhone`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: windowsPhone
      }
    }
    assert(getOS(shadowWindow) === "windowsPhone")
  })

  test(`getOS() windowsPhone`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: windowsPhone
      }
    }
    assert(getOS(shadowWindow) === "windowsPhone")
  })
  test(`getOS() ios`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: ios
      }
    }
    assert(getOS(shadowWindow) === "ios")
  })
  test(`getOS() android`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: android
      }
    }
    assert(getOS(shadowWindow) === "android")
  })

  test(`getOS() macOs`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: macOs
      }
    }
    assert(getOS(shadowWindow) === "MacOSX")
  })

  test(`getOS() windows`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: windows
      }
    }
    assert(getOS(shadowWindow) === "windows")
  })

  test(`getOS() linux`, function () {
    let shadowWindow: IWindow = {
      navigator: {
        userAgent: linux
      }
    }
    assert(getOS(shadowWindow) === "linux")
  })


  test(`getOS() not fund`, function () {
    assert(getOS() === "not fund")
  })
})