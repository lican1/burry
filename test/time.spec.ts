/*
 * @Author: 李灿
 * @Date: 2021-12-20 19:16:04
 * @Description: 时间相关 case
 * @FilePath: /burries/test/time.spec.ts
 */

import { isLeapYear, isSameDay, monthDays } from "../src/index"
import assert from "power-assert"
describe('Time API:', function () {

    describe('#isLeapYear()', function () {
        test(`isLeapYear(2008) should return true`, function () {
            assert(isLeapYear(2008))
        });
        test(`isLeapYear(3000) should return false`, function () {
            assert(!isLeapYear(3000))
        });
        test(`isLeapYear(2012) should return true`, function () {
            assert(isLeapYear(2012))
        });
        test(`isLeapYear(2016) should return true`, function () {
            assert(isLeapYear(2016))
        });
        test(`isLeapYear(2020) should return true`, function () {
            assert(isLeapYear(2020))
        });
        test(`isLeapYear(2024) should return true`, function () {
            assert(isLeapYear(2024))
        });
        test(`isLeapYear(2017) should return false`, function () {
            assert.notEqual(isLeapYear(2017), true)
        });
        test(`isLeapYear(2018) should return false`, function () {
            assert.notEqual(isLeapYear(2018), true)
        });
        test(`isLeapYear(2019) should return false`, function () {
            assert.notEqual(isLeapYear(2019), true)
        });
    });

    describe('#isSameDay()', function () {
        test(`isSameDay(new Date()) should return true`, function () {
            assert(isSameDay(new Date()) === true)
        });
        test(`isSameDay(new Date(), new Date(new Date().getTime() - 86400000)) should return false`, function () {
            assert(isSameDay(new Date(), new Date(new Date().getTime() - 86400000)) === false)
        });
    });

    describe('#monthDays()', function () {
        test(`monthDays(new Date('2019/10/08')) should return 31`, function () {
            assert(monthDays(new Date('2019/10/08')) === 31)
        });
        test(`monthDays(new Date('2019/02/08')) should return 28`, function () {
            assert(monthDays(new Date('2019/02/08')) === 28)
        });
        test(`monthDays(new Date('2020/02/08')) should return 29`, function () {
            assert(monthDays(new Date('2020/02/08')) === 29)
        });
        test(`monthDays(new Date('2020/4/1')) should return 30`, function () {
            assert(monthDays(new Date('2020/4/1')) === 30)
        });
    })
})