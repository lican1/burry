/*
 * @Author: 李灿
 * @Date: 2021-12-20 19:16:04
 * @Description: url 相关 Case
 * @FilePath: /burries/test/url.spec.ts
 * 
 * 
 * 在 jsdom 环境中修改 window.location.search 模拟特定的 search会报错。解决办法：
 * 
 * 1. 通过 JSdom 提供的 formatURL 来创建新的 url 来避免全局修改 URL 带来的副作用 (只是在初始化的时候修改一次，总不能写一个测试 case 就去修改一个
 * 
 * formatUrl，此方案就 pass)
 * 
 * 
 * 2. 通过 jest 文档提供的 mock 方法（非 npm mockjs）。对于一些顽固的属性(不可配置修改，只读属性这种)，先删除（ts 可能会报错,断言为 any 即可删除）
 * 
 * 。删除之后再去模拟这个方法的实现。案例是添加 location 的 changeSearch 方法，去修改 location.search
 * 
 * 
 * 
 * 3. 为什么不直接去修改 location.search ?   location 不可读写，location.search/hash 是可写的
 */

import { getRequest } from "../src/index"
import assert from "power-assert"

describe('Url API:', function () {
    describe('#getRequest()', function () {
        const _window = window
        beforeAll(function () {

            delete (window as any).location;
            window.location = { changeSearch: jest.fn((x) => window.location.search = x) } as any as Location;
            (window.location as any).changeSearch(`?name=lee`)

        })
        test(`代入参数的情况，会把当前参数当查询字符串去查询`, function () {
            assert(getRequest("name", "?id=10&name=lee") === 'lee')
        });

        test(`不代入参数的情况，回去查询 location.search`, function () {

            assert(getRequest("name") === 'lee')
        });


        test(`测试无查询参数的情况，应该返回''`, function () {

            assert(getRequest("name1") === '')
        });


        afterAll(function () {
            window = _window
        })
    });
})