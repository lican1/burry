/*
 * @Author: 李灿
 * @Date: 2021-11-30 17:39:15
 * @Description: 增加删减 class 测试用例
 * 1. 使用钩子创造前置条件-判断 dom 上有没有 class
 */
import { addClass, hasClass, removeClass } from "../src/index"
import assert from "power-assert"

describe("Class API:", function () {
  describe("#addClass()：添加 class", function () {
    let $ele: HTMLElement
    beforeAll(function () {
      $ele = document.createElement("div")
      document.body.appendChild($ele)
    })
    test(`addClass($ele, 'test') should return true`, function () {
      addClass($ele, "test")
      assert(hasClass($ele, "test"))
    })
    afterAll(function () {
      document.body.removeChild($ele)
    })
  })

  describe("#hasClass()", function () {
    let $ele: HTMLElement
    beforeAll(function () {
      $ele = document.createElement("div")
      document.body.appendChild($ele)
      addClass($ele, "test")
    })
    test(`hasClass($ele, 'test') should return true`, function () {
      assert(hasClass($ele, "test"))
    })
    test(`hasClass($ele, 'test') should return false`, function () {
      assert(!hasClass($ele, "test2"))
    })
    afterAll(function () {
      document.body.removeChild($ele)
    })
  })

  describe("#removeClass()", function () {
    let $ele: HTMLElement
    beforeAll(function () {
      $ele = document.createElement("div")
      document.body.appendChild($ele)
      addClass($ele, "test")
    })
    test(`removeClass($ele, 'test') should return false`, function () {
      removeClass($ele, "test")
      assert.notEqual(hasClass($ele, "test"), true)
    })
    test(`removeClass($ele, 'test') should return false`, function () {
      removeClass($ele, "test")
      assert.notEqual(hasClass($ele, "test"), true)
      addClass($ele, "newTest")
      assert(hasClass($ele, "newTest"))
      removeClass($ele, "newTest")
      assert.notEqual(hasClass($ele, "newTest"), true)
    })
    afterAll(function () {
      document.body.removeChild($ele)
    })
  })
})
