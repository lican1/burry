/*
 * @Author: 李灿
 * @Date: 2021-12-20 19:16:04
 * @Description: storage 测试 case
 * @FilePath: /burries/test/storage.spec.ts
 * 
 * 
 * 
 * 某些时候会用到钩子 beforeAll, afterAll。编写storage 相关 Case的时候
 */
import { local, session } from "../src/index"
import assert from "power-assert"
describe('webStorage API: window.localStorage/ window.sessionStorage 的存取', function () {
  describe('#local()', function () {
    beforeAll(function () {
      local('user', {
        name: 'local',
        age: 23
      })
    })

    test(`读取 local`, function () {
      const user = local('user')
      assert(user.name === 'local')
      assert(user.age === 23)
    });

    test(`清除 local 的 user 对象`, function () {
      local('user', '')
      assert(window.localStorage.getItem('user') === null)
    });

    test(`读取 local 中不存在的对象将会返回 null`, function () {
      assert(local('abcdefg') === null)
    });

    afterAll(function () {
      window.localStorage.clear()
    })
  });
});


describe('webStorage API: window.localStorage/ window.sessionStorage 的存取', function () {
  describe('#session()', function () {
    beforeAll(function () {
      session('user', {
        name: 'session',
        age: 23
      })
    })

    test(`读取 session`, function () {
      const user = session('user')
      assert(user.name === 'session')
      assert(user.age === 23)
    });

    test(`读取 session 中不存在的对象将会返回 null`, function () {
      assert(session('abcdefg') === null)
    });

    test(`清除 session 的 user 对象`, function () {
      session('user', '')
      assert(window.sessionStorage.getItem('user') === null)
    });

    afterAll(function () {
      window.sessionStorage.clear()
    })
  });
});