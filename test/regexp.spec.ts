/*
 * @Author: 李灿
 * @Date: 2021-12-20 19:16:04
 * @Description: 正则表达式测试 Case
 * @FilePath: /burries/test/regexp.spec.ts
 * 
 * 测试时，不仅要测试边界情况，还要顾及到随机情况。使用 mockjs 模拟随机情况,会有意外收获，第一次跑测试不报错
 * 后边再跑测试说不定就出错了
 * 
 * 比如：判断代入的字符串是不是只包含数字。当 正则是 0-9 开头，0-9结尾。但是 mock 时，出来的随机数刚好出来一个
 * 负数。期望返回 true，但是却返回了 false
 * 
 * 
 * 
 */
import { isEmail, isIdCard, isInt, isNumber, isPhoneNum, isSpecialCode, isUrl } from "../src/index"
import assert from "power-assert"
import Mock from "mockjs"
const Random = Mock.Random

describe("Regexp API:", function () {

  describe("校验邮箱: ", function () {
    test('代入空字符串', function () {
      assert(isEmail("") === false);
    });
    test('代入非邮箱', function () {
      assert(isEmail("123498032") === false);
    });
    test('代入qq邮箱', function () {
      assert(isEmail("791253@qq.com") === true);
    });
    test('代入左右带空格的qq邮箱', function () {
      assert(isEmail("  791253@qq.com   ") === true);
    });
    test('代入网易邮箱', function () {
      assert(isEmail("791253@163.com") === true);
    });

    test('代入随机 mock 邮箱', function () {
      const mail = Random.email()
      assert(isEmail(mail) === true);
    });
  });

  describe("是否是身份证号", function () {
    test('代入空字符串', function () {
      assert(isIdCard("") === false);
    });
    test('代入数字身份证号', function () {
      const id = 622224188203234033
      assert(isIdCard(id) === true);
    });
    test('代入字符串身份证号', function () {
      const id = '622224188203234033'
      assert(isIdCard(id) === true);
    });
    test('代入左右带空格的字符串身份证号', function () {
      const id = '   622224188203234033   '
      assert(isIdCard(id) === true);
    });
    test('代入非身份证号', function () {
      const id = '1232622224188203234033'
      assert(isIdCard(id) === false);
    });

    test('代入随机身份证号', function () {
      assert(isIdCard(Random.id()) === true);
    });
  });

  describe("是否是数字字符串", function () {
    test('代入空字符串', function () {
      assert(isNumber("") === false);
    });
    test('代入数字', function () {
      const id = 1232131231231231
      assert(isNumber(id) === true);
    });
    test('代入数字字符串，左右带空格', function () {
      const id = '  622224188203234033 '
      assert(isNumber(id) === true);
    });
    test('代入非数字', function () {
      const id = '的几点开始几点开始'
      assert(isNumber(id) === false);
    });
    test('代入随机数字', function () {
      const id = Random.integer()
      assert(isNumber(id) === true);
    });

    let float = Random.float(60, 100, 3, 5)
    test(`代入浮点数 - ${float}`, function () {
      assert(isNumber(float) === false);
    });
    test('代入负数', function () {
      assert(isNumber(Random.integer(-1000, -100)) === true);
    });
  });

  describe("是否是电话号码", function () {
    test('代入空字符串', function () {
      assert(isPhoneNum("") === false);
    });
    test('代入带86的电话号码 ', function () {
      const str = 8618882324234
      assert(isPhoneNum(str) === true);
    });
    test('代入带空格的电话号码', function () {
      const phone = '  19056323241 '
      assert(isPhoneNum(phone) === true);
    });
    test('代入正确的电话号码', function () {
      assert(isPhoneNum("19056323241") === true);
    });

    test('代入错误的电话号码', function () {
      assert(isPhoneNum(Random.sentence()) === false);
    });
    test('代入错误的电话号码', function () {
      assert(isPhoneNum(Random.float()) === false);
    });
  });

  describe("#isUrl()", function () {
    test('isUrl("https://www.baidu.com/s?wd=www.slane.cn&rsv_spt=1") should return true ', function () {
      assert(isUrl("https://www.baidu.com/s?wd=www.slane.cn&rsv_spt=1"));
    });
    test('isUrl("http://www.baidu.com/s?wd=www.slane.cn&rsv_spt=1") should return true ', function () {
      assert(isUrl("http://www.baidu.com/s?wd=www.slane.cn&rsv_spt=1"));
    });
    test('带空格的 URL', function () {
      assert(isUrl("  www.baidu.com  "));
    });
    test('只含域名，不含协议的 url', function () {
      assert(isUrl("baidu.com"));
    });
    test('错误的 url', function () {
      assert(!isUrl("http://baiducom"));
    });
  });

  describe("判断是不是一个正整数", function () {
    test('代入空字符串 ', function () {
      assert(isInt("") === false);
    });
    test("代入数字", function () {
      assert(isInt(622224188203234033) === true);
    });
    test("代入数字字符串 ", function () {
      const str = '622224188203234033'
      assert(isInt(str) === true);
    });

    test("代入加空格的数字字符串 ", function () {
      const str = '   622224188203234033   '
      assert(isInt(str) === true);
    });
    test('代入 0 ', function () {
      assert(!isInt(0));
    });
    test("代入1", function () {
      assert(isInt(1));
    });
    test("代入负数 ", function () {
      assert(!isInt(-1));
    });
    test("代入浮点数 ", function () {
      assert(!isInt(1.8));
    });
    test("代入随机整数 ", function () {
      const n = Random.integer()
      if (n > 0) {
        assert(isInt(n) === true);

      } else {
        assert(isInt(n) === false);
      }
    });
  });

  describe("#isSpecialCode() 判断是否含有特殊字符", function () {
    test('isSpecialCode("埃美柯") 应该返回 false ', function () {
      assert(!isSpecialCode("埃美柯"));
    });

    test('isSpecialCode("埃美柯.-") 应该返回 true ', function () {
      assert(isSpecialCode("埃美柯.-"));
    });
  });
});
