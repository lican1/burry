/*
 * @Author: 李灿
 * @Date: 2021-11-30 18:20:26
 * @Description:cookie 测试案例
 */
import { getCookie, setCookie, removeCookie } from "../src/index"
import assert from "power-assert"
describe("Cookie API:", function () {
  describe("#getCookie()", function () {
    beforeAll(function () {
      setCookie("test", "getTestValue")
    })
    test(`getCookie('test') should return getTestValue`, function () {
      assert(getCookie("test") === "getTestValue")
    })
    test(`getCookie('empty') should return ''`, function () {
      assert(getCookie("empty") === "")
    })
    afterAll(function () {
      removeCookie("test")
    })
  })

  describe("#removeCookie()", function () {
    beforeAll(function () {
      setCookie("test", "removeTestValue")
    })
    test(`removeCookie('test') should return false`, function () {
      removeCookie("test")
      assert.notEqual(getCookie("test") === "removeTestValue", true)
    })
  })

  describe("#setCookie()", function () {
    test(`getCookie('test', 'setCookie') should return true`, function () {
      setCookie("test", "setCookie")
      assert(getCookie("test") === "setCookie")
    })
    afterAll(function () {
      removeCookie("test")
    })
  })
})
