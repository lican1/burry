/*
 * @Author: 李灿
 * @Date: 2021-12-20 10:49:17
 * @Description: 文件系列测试 case
 * @FilePath: /burries/test/file.spec.ts
 * 
 * 1. input 选择框去选择文件再上传，需要人为操作。代码模拟不了人为的操作, 故用 new File 构造函数去构建文件
 * 
 * 2. 测试文件大小时，由于对于文件类型来说，文件大小是一个只读属性,不能通过构造函数去模拟大小，只能通过真实文件去模拟
 *    通过 fs.readFile 去读真实文件， 
 */

import fs from "fs"
import path from "path"
import { isAssetTypeAnImage, validateSize } from "../src/index"
import assert from "power-assert"

describe("File API:", function () {
    // png 格式
    test(`文件格式 png，测试是否是图片类型`, function () {
        const file = new File(['1', '2', '3'], 'a.png')
        assert(isAssetTypeAnImage(file) === true)
    })

    // jpg 格式
    test(`文件格式 jpg，测试是否是图片类型`, function () {
        const file = new File(['1', '2', '3'], 'a.jpg')
        assert(isAssetTypeAnImage(file) === true)
    })

    // jpeg 格式
    test(`文件格式 jpeg，测试是否是图片类型`, function () {
        const file = new File(['1', '2', '3'], 'a.jpeg')
        assert(isAssetTypeAnImage(file) === true)
    })

    // bmp 格式
    test(`文件格式 bmp，测试是否是图片类型`, function () {
        const file = new File(['1', '2', '3'], 'a.bmp')
        assert(isAssetTypeAnImage(file) === true)
    })

    // gif 格式
    test(`文件格式 gif，测试是否是图片类型`, function () {
        const file = new File(['1', '2', '3'], 'a.gif')
        assert(isAssetTypeAnImage(file) === true)
    })

    // 测试文件大小
    test(`文件大小，1M`, function () {
        const buffer = fs.readFileSync(path.resolve(__dirname, '../public/image.png'))
        const file = new File([buffer], 'image.img')
        assert(validateSize(file, 1) === true)
    })
})