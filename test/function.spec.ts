/*
 * @Author: 李灿
 * @Date: 2021-12-22 17:32:28
 * @Description: 函数节流防抖测试 case
 * @FilePath: /burries/test/function.spec.ts
 * 
 *1.  思路：用定时器，对于发防抖，写多个执行函数，测试是不是只执行了最后一次。对于节流，定时器里每隔一毫秒
 * 
 * 放一个时间，待到一段时间后，测试是不是执行了相应的次数。这个时间范围选的要合适，节流区间为300 ms,如果
 * 要执行两次，断言的节点应该在 >600  x < 900这个范围，因为定时器自身机制，并不会在规定时间立即执行。需要等待
 * 
 * 线程空闲，且时间循环机制收集到这个结果之后才会把结果反映出来
 * 
 * 
 * 2. 测试异步代码，在回调函数里面代入 done，一定要显式的调用 done 告知 jest 测试完毕。否则将会一直等待至默认
 * 的超时时间(5000 ms)，然后抛出错误
 * 
 * 
 * 3. 测试once,触发多次函数，看执行了多少次
 */
import { debounce, throttle, once } from "../src/index"
import assert from "power-assert"

describe('Function API:', function () {

    describe('防抖函数:  高频事件，短暂延时之后，只触发最后一次, ', function () {
        beforeAll(function () {
            jest.setTimeout(10000)
        })
        test(`debounce 函数 - 默认延时时间为 300ms`, function (done) {
            var test = jest.fn();
            var debounced = debounce(test);
            debounced();
            debounced();
            debounced();
            debounced();
            setTimeout(() => {
                expect(test).toHaveBeenCalledTimes(1);
                done()
            }, 500)
        })
        test(`debounce 函数 - 默认延时时间为 300ms， 代入 delay 为 100ms,在200 ms 的时候测试结果`, function (done) {
            const object = {
                num: 0,
                add: function () {
                    this.num = this.num + 1
                }
            }
            const de = debounce(object.add, 100).bind(object)
            de()
            de()
            de()
            de()
            setTimeout(() => {
                assert(object.num === 1)
                done()
            }, 200)
        })

        test(`debounce 函数 - 绑定 this`, function (done) {
            const object = {
                num: 0,
                add: function () {
                    this.num = this.num + 1
                }
            }
            const de = debounce(object.add).bind(object)
            de()
            de()
            de()
            de()
            setTimeout(() => {
                assert(object.num === 1)
                done()
            }, 500)
        })


        test(`debounce 函数 - 绑定 this 与参数`, function (done) {
            const object = {
                num: 0,
                add: function (n: number) {
                    this.num = this.num + n
                }
            }
            const de = debounce(object.add).bind(object)
            de(1)
            de(2)
            de(3)
            de(4)
            setTimeout(() => {
                assert(object.num === 4)
                done()
            }, 500)
        })
    });

    // 节流函数
    describe('节流函数： 高频触发的事件，每隔一段时间执行一次', function () {
        test(`throttle 函数 - 默认延时时间为 300ms`, function (done) {
            var test = jest.fn();
            var throttled = throttle(test);
            let interval = setInterval(() => {
                throttled();
            }, 1)
            setTimeout(() => {
                clearInterval(interval)
                expect(test).toHaveBeenCalledTimes(3); // 进入节流的时候立马执行了一次
                done()
            }, 800)
        })


        test(`throttle 函数 - 绑定 this`, function (done) {
            const object = {
                num: 0,
                add: function () {
                    this.num = this.num + 1
                }
            }
            const throttled = throttle(object.add).bind(object)
            let interval = setInterval(() => {
                throttled();
            }, 1)
            setTimeout(() => {
                clearInterval(interval)
                assert(object.num === 3)
                done()
            }, 800)
        })
        test(`throttle 函数 - 默认延时时间为 300ms, 代入 delay 为 100ms,在200 ms 的时候测试结果`, function (done) {
            const object = {
                num: 0,
                add: function () {
                    this.num = this.num + 1
                }
            }
            const de = throttle(object.add, 100).bind(object)
            let interval = setInterval(de, 1)
            setTimeout(() => {
                clearInterval(interval)
                assert(object.num === 2)
                done()
            }, 200)
        })

        test(`throttle 函数 - 绑定 this 与参数`, function (done) {
            const object = {
                num: 0,
                add: function (n: number) {
                    this.num = this.num + n
                }
            }
            const de = throttle(object.add).bind(object)
            let interval = setInterval(() => {
                de(2);
            }, 1)

            setTimeout(() => {
                clearInterval(interval)

                assert(object.num === 6)
                done()
            }, 800)
        })


    });


    // once 
    describe('once 函数，无论调用多少次，都只执行一次的函数', function () {
        test(`once 测试 Case, 定时器中执行`, function (done) {
            var test = jest.fn();
            var throttled = once(test);
            let interval = setInterval(() => {
                throttled();
            }, 1)
            setTimeout(() => {
                clearInterval(interval)
                expect(test).toHaveBeenCalledTimes(1);
                done()
            }, 800)
        })


        test(`once 测试 Case, 调用多次`, function (done) {
            var test = jest.fn();
            var throttled = once(test);
            throttled();
            throttled();
            throttled();
            throttled();

            setTimeout(() => {
                expect(test).toHaveBeenCalledTimes(1);
                done()
            }, 800)
        })




    });


});

