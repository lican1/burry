/*
 * @Author: 李灿
 * @Date: 2021-12-20 18:57:55
 * @Description: 随机数、随机颜色测试 case
 * @FilePath: /burries/test/random.spec.ts
 */

import { randomColor, randomNum } from "../src/index"
import assert from "power-assert"
describe('Random API:', function () {
    describe('#randomColor()', function () {
        test(`/^#[0-9a-fA-F]$/.test(randomColor()) should return true`, function () {
            assert(/^#[0-9a-fA-F]{6}$/.test(randomColor()))
        });
    });

    describe('#randomNum()', function () {
        test(`10 <= randomNum(10, 1000) <= 100 should return true`, function () {
            let num = randomNum(10, 1000)
            assert(num <= 1000 && num > 10)
        });

        // 测试 0.1 ~ 1.2 之间只能取得 1，不能为 0
        test(`1 === randomNum(0.1, 1.2) should return true`, function () {
            let num = randomNum(0.1, 1.2)
            assert(num === 1)
        });
    });
})