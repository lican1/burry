/*
 * @Author: 李灿
 * @Date: 2021-12-20 17:39:57
 * @Description: object 类型的测试 case
 * @FilePath: /burries/test/object.spec.ts
 */
import { getTypeof } from "../src/index"
import assert from "power-assert"

describe("获取对象类型 API", function () {
    test('代入undefined参数情况， 返回 undefined', function () {
        assert(getTypeof(undefined) === 'undefined')
    })

    test('对象类型', function () {
        const object = { name: '123' }
        assert(getTypeof(object) === 'object')
    })

    test('数组类型', function () {
        const object = [1, 2]
        assert(getTypeof(object) === 'array')
    })

    test('函数类型', function () {
        const object = () => { }
        assert(getTypeof(object) === 'function')
    })

    test('正则类型', function () {
        const object = /a/g
        assert(getTypeof(object) === 'regExp')
    })

    test('null类型', function () {
        const object = null
        assert(getTypeof(object) === 'null')
    })

    test('boolean类型', function () {
        const object = true
        assert(getTypeof(object) === 'boolean')
    })

    test('Symbol 类型', function () {
        const object = Symbol('sys')
        assert(getTypeof(object) === 'symbol')
    })

    test('string 类型', function () {
        const object = '123'
        assert(getTypeof(object) === 'string')
    })


    test('number 类型', function () {
        const object = 123
        assert(getTypeof(object) === 'number')
    })

    test('map 类型', function () {
        const object = new Map([
            [1, { b: true }]
        ])
        assert(getTypeof(object) === 'map')
    })

    test('weakMap 类型', function () {
        const object = new WeakMap([
        ])
        assert(getTypeof(object) === 'weakMap')
    })

    test('set 类型', function () {
        const object: Set<number> = new Set([])
        assert(getTypeof(object) === 'set')
    })

    test('weakSet 类型', function () {
        const object = new WeakSet([])
        assert(getTypeof(object) === 'weakSet')
    })
})