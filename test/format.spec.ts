/*
 * @Author: 李灿
 * @Date: 2021-12-20 14:15:37
 * @Description: 数据格式化测试 case
 * @FilePath: /burries/test/format.spec.ts
 * 
 * 
 * 
 * 边界情况 完善方法-未带入参数的情况
 * 
 * 兼容数据类型-对于时间对象来说，兼容时间戳字符串的情况
 */
import { filterCard, filterPhone, commodify, formatDate, formatTime, digitUppercase } from "../src/index"
import assert from "power-assert"
describe("格式化身份证 API:", function () {
    // 身份证号
    test(`代入数字格式身份证号`, function () {
        const id = '632323190605269601'
        assert(filterCard(id) === '6323231906****9601')
    })
    test(`代入字符串格式身份证号`, function () {
        const id = '632323190605269601'
        assert(filterCard(id) === '6323231906****9601')
    })
    test(`代入有首尾空格的身份证号`, function () {
        const id = '  632323190605269601   '
        assert(filterCard(id) === '6323231906****9601')
    })

    test(`代入非正确格式的身份证号`, function () {
        const id = '错误格式的身份证号'
        assert(filterCard(id) === '')
    })

    test(`代入非正确格式的身份证号`, function () {
        const id = ''
        assert(filterCard(id) === '')
    })
})

describe("格式化电话号码 API:", function () {
    test(`代入字符串格式电话`, function () {
        const id = '18327787663'
        assert(filterPhone(id) === '183****7663')
    })
    test(`代入有首尾空格的电话号码`, function () {
        const id = '  18327787663   '
        assert(filterPhone(id) === '183****7663')
    })

    test(`代入错误格式的电话号码`, function () {
        const id = ' 12345678910'
        assert(filterPhone(id) === '')
    })
    test(`代入错误格式的电话号码`, function () {
        const id = '  '
        assert(filterPhone(id) === '')
    })
})


describe("给数字中间每 3 位加逗号 API:", function () {
    test(`代入万以下数字`, function () {
        const id = 12908
        assert(commodify(id) === '12,908')
    })
    test(`代入大数字`, function () {
        const id = '18327787663'
        assert(commodify(id) === '18,327,787,663')
    })

    test(`代入带首尾空格的`, function () {
        const id = ' 12345678910'
        assert(commodify(id) === '12,345,678,910')
    })

    test(`代入错误的数据`, function () {
        const id = ' 123a45678910'
        assert(commodify(id) === '')
    })

    test(`代入错误的数据`, function () {
        const id = ''
        assert(commodify(id) === '')
    })
})

describe("formatDate 日期格式化 API:", function () {
    test(`代入空字符串`, function () {
        const date = ''
        assert(formatDate(date, 'YYYY-MM-DD') === '')
    })
    test(`代入时间戳数字`, function () {
        const data = 1639985036597
        assert(formatDate(data, 'YYYY-MM-DD') === '2021-12-20')
    })

    test(`代入时间戳字符串`, function () {
        const data = '1639985036597'
        console.log(formatDate(data, 'YYYY-MM-DD'));

        assert(formatDate(data, 'YYYY-MM-DD') === '2021-12-20')
    })

    test(`代入时间对象`, function () {
        const data = new Date('2021/12/20')
        assert(formatDate(data, 'YYYY-MM-DD') === '2021-12-20')
    })

    test(`代入表示时间的字符串`, function () {
        const data = '2021/12/20'
        assert(formatDate(data, 'YYYY-MM-DD') === '2021-12-20')
    })

    test(`代入表示时间的字符串`, function () {
        const data = '2021-12-20'
        assert(formatDate(data, 'YYYY/MM/DD') === '2021/12/20')
    })
})

describe("formatTime 计算剩余时间:", function () {
    test(`代入空字符串`, function () {
        const date = ''
        const { day, hour, min, seconds } = formatTime(date)
        assert(day === 0)
        assert(hour === 0)
        assert(min === 0)
        assert(seconds === 0)
    })

    test(`代入非法参数`, function () {
        const date = '角度啥的是 12212121'
        const { day, hour, min, seconds } = formatTime(date)
        assert(day === 0)
        assert(hour === 0)
        assert(min === 0)
        assert(seconds === 0)
    })

    test(`代入字符串参数, 1分钟`, function () {
        const date = '60000'
        const { day, hour, min, seconds } = formatTime(date)
        assert(day === 0)
        assert(hour === 0)
        assert(min === 1)
        assert(seconds === 0)
    })

    test(`代入字符串参数, 1分钟1 秒`, function () {
        const date = `${60000 + 1000} `
        const { day, hour, min, seconds } = formatTime(date)
        assert(day === 0)
        assert(hour === 0)
        assert(min === 1)
        assert(seconds === 1)
    })

    test(`代入字符串参数, 1分钟1 秒,首尾带空格`, function () {
        const date = `             ${60000 + 1000}         `
        const { day, hour, min, seconds } = formatTime(date)
        assert(day === 0)
        assert(hour === 0)
        assert(min === 1)
        assert(seconds === 1)
    })

    test(`代入字符串参数, 1小时1分钟1 秒`, function () {
        const date = `${60000 * 60 + 60000 + 1000} `
        const { day, hour, min, seconds } = formatTime(date)
        assert(day === 0)
        assert(hour === 1)
        assert(min === 1)
        assert(seconds === 1)
    })

    test(`代入数字参数, 1 天 1小时1分钟1 秒`, function () {
        const date = `${60000 * 60 * 24 + 60000 * 60 + 60000 + 1000}`
        const { day, hour, min, seconds } = formatTime(date)
        assert(day === 1)
        assert(hour === 1)
        assert(min === 1)
        assert(seconds === 1)
    })

    test(`代入数字参数, 计算两个日期对象之间的差 `, function () {
        const date1 = new Date('2021/12/20')
        const date2 = new Date('2021/12/22 2:22:22')
        const _r = Math.abs(+date2 - (+date1))
        const { day, hour, min, seconds } = formatTime(_r)
        assert(day === 2)
        assert(hour === 2)
        assert(min === 22)
        assert(seconds === 22)
    })

})

describe('现金金额转大写 API:', function () {
    describe('#digitUppercase()', function () {
        test(`digitUppercase(9999999999) === '玖拾玖亿玖仟玖佰玖拾玖万玖仟玖佰玖拾玖元整' should return true`, function () {
            assert(digitUppercase(9999999999) === "玖拾玖亿玖仟玖佰玖拾玖万玖仟玖佰玖拾玖元整")
        });

        test(`digitUppercase(0) === '零元整' should return true`, function () {
            assert(digitUppercase(0) === "零元整")
        });

        test(`digitUppercase(-235423545) === '欠贰亿叁仟伍佰肆拾贰万叁仟伍佰肆拾伍元整' should return true`, function () {
            assert(digitUppercase(-235423545) === "贰亿叁仟伍佰肆拾贰万叁仟伍佰肆拾伍元整")
        });
    });
})
