/*
 * @Author: 李灿
 * @Date: 2021-11-30 11:07:59
 * @Description:
 */
import path from "path"

import { terser } from "rollup-plugin-terser" // 压缩打包后的代码
import resolve from "rollup-plugin-node-resolve" // 依赖引用插件
import commonjs from "rollup-plugin-commonjs" // commonjs模块转换插件
import ts from "rollup-plugin-typescript2"
import packageJson from "./package.json"

const getPath = _path => path.resolve(__dirname, _path)
const extensions = [".js", ".ts", ".tsx"]
// ts
const tsPlugin = ts({
  tsconfig: getPath("./tsconfig.json"), // 导入本地ts配置
  extensions,
})

export default {
  // 核心选项
  input: getPath("src/index.ts"), // 必须

  output: {
    // 必须 (如果要输出多个，可以是一个数组)
    // 核心选项
    file: packageJson.main, // 必须
    name: packageJson.name,
    format: "umd", // 必须
  },

  // 集成 typescript
  plugins: [resolve(extensions), commonjs(), tsPlugin, terser()],
}
