###
 # @Author: 李灿
 # @Date: 2021-12-22 08:14:17
 # @Description: git 提交
 # @FilePath: /burries/git.sh
### 
 #!/bin/bash
 git_add(){
    echo ">>>>>> 执行 git add 之前,本地文件状态如下 <<<<<<"
    git status 
    statusResult=$(git status)
    no_change="nothing to commit"

    contains_str "$statusResult" "$no_change"

    if [[ $? == 1 ]]; then
        echo "=== 当前没有新增或者修改的文件 ==="
        # git_push
        exit
    fi

    read -p "是否确定add？Y|N : " add_params
    if [[ $add_params == "Y" || $add_params == "y" ]]; then 
            git add .
    else 
        exit 
    fi     
}

git_commit(){
     echo ">>>>>> 执行 git commit 之前,本地文件状态如下 <<<<<<"
     git status 
     read -p "是否确定commit？Y|N : " commit_params
     if [[ $commit_params == "Y" || $commit_params == "y" ]] ; then
             read -p "请输入commit信息: " commit_msg
             if [ -z $commit_msg  ] ; then 
                 git commit -m "git commit by $author" .
             else
                 git commit -m $commit_msg .    
             fi
     elif [[ $commit_params == "N" || $commit_params == "n" ]] ; then 
          exit 
     else 
         exit    
     fi
}


git_add;
git_commit;