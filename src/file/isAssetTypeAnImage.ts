/*
 * @Author: 李灿
 * @Date: 2021-07-27 12:03:52
 * @Description:isAssetTypeAnImage 判断文件类型是不是图片
 * @FilePath: /burries/src/file/isAssetTypeAnImage.ts
 */
/**
 * 判断文件类型是不是图片
 * @param {File} file 文件对象
 * @return {*} true 表示是图片  false 表示不是图片
 */
function isAssetTypeAnImage(file: File): boolean {
  const fileName = file.name;
  const index = fileName.lastIndexOf(".");
  const ext = fileName.slice(index + 1);
  return ["png", "jpg", "jpeg", "bmp", "gif"].indexOf(ext.toLowerCase()) !== -1;
}

export default isAssetTypeAnImage
