/*
 * @Author: 李灿
 * @Date: 2021-07-27 12:05:11
 * @Description:判断文件大小是不是超出范围
 * @FilePath: /burries/src/file/validateSize.ts
 */

/**
 * 判断文件大小是不是超出范围
 * @param {*} file 判断文件大小是不是超出范围
 * @param {*} maxSize 最大值 单位 M
 * @return {*} Boolean true 表示合法的文件大小
 */
function validateSize(file: File, maxSize: number): boolean {
  const fileSize = file.size;
  return fileSize < 1024 * maxSize * 1024;
}

export default validateSize
