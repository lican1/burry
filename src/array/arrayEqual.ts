/**
 *  比较两个数组是否相等
 * @param  arr1 参与比较的数组
 * @param  arr2 参与比较的数组
 * @return true 代表相等，false 代表不相等
 */
function arrayEqual(arr1: Array<any>, arr2: Array<any>): boolean {
  if (arr1 === arr2) return true
  if (arr1.length != arr2.length) return false
  for (var i = 0; i < arr1.length; ++i) {
    if (arr1[i] !== arr2[i]) return false
  }
  return true
}

export default arrayEqual
