/*
 * @Author: 李灿
 * @Date: 2021-07-19 10:43:26
 * @Description:localStorage 存取方法
 * @FilePath: /burries/src/storage/local.ts
 */
/**
 * localStorage 存取方法
 * @param {*} key 存入对象的key
 * @param {*} val 存入对象的 Value，可选，存在时代表存，不存在时代表读取。代入空字符串时代表清空
 * @return {*}
 */
function local(key: string, val?: any): any {
  if (val) {
    window.localStorage.setItem(key, JSON.stringify(val));
    return val;
  } else {
    if (val === "") {
      window.localStorage.removeItem(key);
    } else {
      if (window.localStorage[key]) {
        const result = window.localStorage.getItem(key)
        return JSON.parse((<string>result));
      } else {
        return null;
      }
    }
  }
}

export default local