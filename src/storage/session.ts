/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: SessionStore 的存取 
 * @FilePath: /burries/src/storage/session.ts
 */
/**
 * SessionStore 的存取 
 * @param {*} key 存入对象的key
 * @param {*} val 存入对象的 Value，可选，存在时代表存，不存在时代表读取。代入空字符串时代表清空
 * @return {*}
 */
function session(key: string, val?: any): any {
  if (val) {
    window.sessionStorage.setItem(key, JSON.stringify(val))
    return val
  } else {
    if (val === '') {
      window.sessionStorage.removeItem(key)
    } else {
      if (window.sessionStorage[key]) {
        const result = window.sessionStorage.getItem(key)
        return JSON.parse((<string>result));
      } else {
        return null
      }
    }
  }
}

export default session