/*
 * @Author: 李灿
 * @Date: 2021-09-01 10:48:26
 * @LastEditTime: 2021-12-21 15:08:27
 * @Description:是否是只包含数字的字符串
 * @FilePath: /burries/src/regexp/isNumber.ts
 */
/**
 * 是否是纯数字字符串，不包含小数点,可包含负号
 * @param {*} value 需要做校验的字符串,会忽略左右空格
 * @return {*} 校验结果
 */
function isNumber(value: string | number | bigint): boolean {
  if (!value) {
    return false
  }
  let val = Math.abs(Number(value))
  return /^[0-9]{1,}$/.test(val.toString().trim());
}
export default isNumber
