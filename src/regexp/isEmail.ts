/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 判断是否为邮箱地址
 * @FilePath: /burries/src/regexp/isEmail.ts
 */
/**
 * 
 *  判断是否为邮箱地址
 * @param  {String}  str 导入的字符串，会忽略前后的空格
 * @return {Boolean} 是否是邮箱
 */
function isEmail(str: string): boolean {
    return /\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/.test(str);
}

export default isEmail