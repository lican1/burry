/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 
 * @FilePath: /burries/src/regexp/isSpecialCode.ts
 */
/**
 * 是否含有特殊字符 
 * @param {*} value
 * @return {*}
 */
function isSpecialCode(value: string): boolean {
  let reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？%+_]")
  return reg.test(value)
}
export default isSpecialCode