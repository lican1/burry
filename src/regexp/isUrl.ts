/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 
 * @FilePath: /burries/src/regexp/isUrl.ts
 */
/**
 * 
 * 判断是否为URL地址, 会忽略前后空格
 * @param  {String} str 
 * @return {Boolean}
 */
function isUrl(str: string): boolean {
    return /[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/i.test(str.trim());
}

export default isUrl