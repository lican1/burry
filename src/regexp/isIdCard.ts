/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 判断是否为身份证号
 * @FilePath: /burries/src/regexp/isIdCard.ts
 */
/**
 * 
 *  判断是否为身份证号
 * @param  {String|Number} string 需要做判断的字符串或者数字，会忽略前后空格
 * @return {Boolean} 是否是身份证号
 */
function isIdCard(string: string | number | bigint): boolean {
    // 删除首尾空格
    const str = string.toString().trim()
    return /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/.test(str)
}

export default isIdCard