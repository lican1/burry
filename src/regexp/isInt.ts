/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 是否是正整数
 * @FilePath: /burries/src/regexp/isInt.ts
 */
/**
 * 是否是正整数
 * @param {*} value 做校验的数字或者字符串，或忽略前后空格
 * @return {*} 判断结果
 */
function isInt(value: number | string | bigint): boolean {
  const val = value.toString().trim()
  return /^[1-9]\d*$/.test(val)
}
export default isInt