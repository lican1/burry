/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 判断是否为手机号
 * @FilePath: /burries/src/regexp/isPhoneNum.ts
 */
/**
 * 
 *  判断是否为手机号
 * @param  {String|Number} str 需要做校验的字符串，会忽略前后空格
 * @return {Boolean} 是否是手机号
 */
function isPhoneNum(str: number | string): boolean {
    const str_ = str.toString().trim()
    return /^(\+?0?86\-?)?1[3456789]\d{9}$/.test(str_)
}

export default isPhoneNum