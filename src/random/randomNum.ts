/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 生成指定范围[min, max]的随机数
 * @FilePath: /burries/src/random/randomNum.ts
 */
/**
 * 
 *  生成指定范围[min, max]的随机整数
 * @param  {Number} min 最小值
 * @param  {Number} max 最大值
 * @return {Number} 
 */
function randomNum(min: number, max: number): number {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export default randomNum
