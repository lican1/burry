/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:37
 * @Description:
 */

/**
 判断元素是否有某个 class
 * 
 * @param {HTMLElement} ele 要参与判断的目标元素
 * @param {string} cls class 名称
 */
export default function hasClass(ele: HTMLElement, cls: string): boolean {
  return new RegExp("(\\s|^)" + cls + "(\\s|$)").test(ele.className)
}
