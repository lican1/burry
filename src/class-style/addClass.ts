import hasClass from "./hasClass"
/**
 为元素添加class
 * 
 * @param {HTMLElement} ele 要添加 class 的目标元素
 * @param {string} cls class 名称
 */
function addClass(ele: HTMLElement, cls: string): void {
  if (!hasClass(ele, cls)) {
    ele.className += " " + cls
  }
}

export default addClass
