import hasClass from "./hasClass"
/**
 为元素移除 class
 * 
 * @param {HTMLElement} ele 要移除 class 的目标元素
 * @param {string} cls 要移除的 class名称
 */
export default function removeClass(ele: HTMLElement, cls: string): void {
  if (hasClass(ele, cls)) {
    var reg = new RegExp("(\\s|^)" + cls + "(\\s|$)")
    ele.className = ele.className.replace(reg, " ")
  }
}
