/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 获取对象的类型
 * @FilePath: /burries/src/object/getTypeof.ts
*/
type KEY = '[object Object]' | '[object Array]' | '[object Function]' | '[object RegExp]' | '[object Null]'
  | '[object Undefined]' | '[object Boolean]' | '[object Boolean]' | '[object Symbol]' | '[object String]' | '[object Number]'
  | '[object Map]' | '[object WeakMap]' | '[object Set]' | '[object WeakSet]'



/**
 * 获取对象的类型
 * @param {any} param 需要判断类型的对象
 * @return {*}
 */
function getTypeof(param: any) {
  const dictionary = {
    '[object Object]': 'object',
    '[object Array]': 'array',
    '[object Function]': 'function',
    '[object RegExp]': 'regExp',
    '[object Null]': 'null',
    '[object Undefined]': 'undefined',
    '[object Boolean]': 'boolean',
    '[object Symbol]': 'symbol',
    '[object String]': 'string',
    '[object Number]': 'number',
    '[object Map]': 'map',
    '[object WeakMap]': 'weakMap',
    '[object Set]': 'set',
    '[object WeakSet]': 'weakSet'
  }
  const key = Object.prototype.toString.call(param)
  return dictionary[key as KEY] || 'unknown'
}

export default getTypeof
