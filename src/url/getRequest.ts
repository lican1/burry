/*
 * @Author: 李灿
 * @Date: 2021-11-30 11:50:23
 * @Description:获取路径上的参数
 */


interface O {
  [prop: string]: any

}
/**
 * 获取路径上的参数
 * @param {string} param 要获取的属性
 * @param {string} search 查询的字符串，可选。默认为 location.search
 * @return {*} 查询的参数值
 */
function getRequest(param: string, search?: string): string {
  var url = search || location.search
  let theRequest: O = {}
  if (url.indexOf("?") != -1) {
    var str = url.slice(1)
    var stars = str.split("&")
    for (var i = 0; i < stars.length; i++) {
      // unescape() 函数可对通过 escape() 编码的字符串进行解码。
      theRequest[stars[i].split("=")[0]] = decodeURIComponent(stars[i].split("=")[1])
    }
  }
  return theRequest[param] || ""
}
export default getRequest
