/*
 * @Author: 李灿
 * @Date: 2021-11-30 11:13:48
 * @Description:
 */
// import JSON from "../package.json"
// export const version: string = `${JSON.version}`

export { default as arrayEqual } from "./array/arrayEqual"
// class-style
export { default as addClass } from "./class-style/addClass"
export { default as hasClass } from "./class-style/hasClass"
export { default as removeClass } from "./class-style/removeClass"
// cookie
export { default as getCookie } from "./cookie/getCookie"
export { default as setCookie } from "./cookie/setCookie"
export { default as removeCookie } from "./cookie/removeCookie"
// device
export { default as getExplore } from "./device/getExplore"
export { default as getOS } from "./device/getOS"
// dom
export { default as windowResize } from "./dom/windowResize"
// file
export { default as isAssetTypeAnImage } from "./file/isAssetTypeAnImage"
export { default as validateSize } from "./file/validateSize"
// format
export { default as commodify } from "./format/commodify"
export { default as filterCard } from "./format/filterCard"
export { default as filterPhone } from "./format/filterPhone"
export { default as formatDate } from "./format/formatDate"
export { default as formatTime } from "./format/formatTime"
export { default as digitUppercase } from "./format/digitUppercase"
// function
export { default as debounce } from "./function/debounce"
export { default as once } from "./function/once"
export { default as throttle } from "./function/throttle"


// object
export { default as getTypeof } from "./object/getTypeof"
// random
export { default as randomNum } from "./random/randomNum"
export { default as randomColor } from "./random/randomColor"
// regxp
export { default as isEmail } from "./regexp/isEmail"
export { default as isIdCard } from "./regexp/isIdCard"
export { default as isInt } from "./regexp/isInt"
export { default as isNumber } from "./regexp/isNumber"
export { default as isPhoneNum } from "./regexp/isPhoneNum"
export { default as isSpecialCode } from "./regexp/isSpecialCode"
export { default as isUrl } from "./regexp/isUrl"

// storage
export { default as local } from "./storage/local"
export { default as session } from "./storage/session"

// time

export { default as isLeapYear } from "./time/isLeapYear"
export { default as isSameDay } from "./time/isSameDay"
export { default as monthDays } from "./time/monthDays"

// url
export { default as getRequest } from "./url/getRequest"
