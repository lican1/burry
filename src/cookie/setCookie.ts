/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:37
 * @Description:
 */
/**
 * 根据name去设置 cookie
 * @param {string} name 设置的 cookie 名称(key)
 * @param {string} value 设置 cookie的值(value)
 * @param {number} days 设置的天数
 */
function setCookie(name: string, value: string, days?: number): void {
  var date = new Date()
  date.setDate(date.getDate() + (days || 0))
  document.cookie = name + "=" + value + ";expires=" + date
}

export default setCookie
