/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:37
 * @Description:
 */
/**
 * 根据name读取cookie
 * @param {string} cookieName 要获取的 cookie 名
 * @return {string} 获取的结果
 */
function getCookie(cookieName: string): string {
  let arr = document.cookie.replace(/\s/g, "").split(";")
  for (let i = 0; i < arr.length; i++) {
    let tempArr = arr[i].split("=")
    if (tempArr[0] == cookieName) {
      // decodeURIComponent() 函数可对 encodeURIComponent() 函数编码的 URI 进行解码。
      return decodeURIComponent(tempArr[1])
    }
  }
  return ""
}
export default getCookie
