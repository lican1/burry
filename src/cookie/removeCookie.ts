/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:37
 * @Description:
 */
import setCookie from "./setCookie"
/**
 * 根据name去删除 cookie
 * @param {string} name 要移除的 cookie 名称
 */
function removeCookie(name: string): void {
  // 设置已过期，系统会立刻删除cookie
  setCookie(name, "1", -1)
}

export default removeCookie
