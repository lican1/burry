/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:37
 * @Description: 获取操作系统类型
 * @FilePath: /burries/src/device/getOS.ts
 */
/**
 * 获取操作系统类型
 * @param _window 含有navigator.userAgent的对象，默认是当前window 
 * @return {*} 当前操作系统类型
 */
function getOS(_window?: {
    navigator: {
        userAgent: string
    }
} | Window) {
    let $window = _window ? _window : window
    var userAgent = $window.navigator.userAgent.toLowerCase();
    const rule = userAgent
    if (/iphone/i.test(rule) || /ipad/i.test(rule) || /ipod/i.test(rule)) return 'ios'
    if (/android/i.test(rule)) return 'android'
    if (/win/i.test(rule) && /phone/i.test(userAgent)) return 'windowsPhone'
    if (/mac/i.test(rule)) return 'MacOSX'
    if (/windows/i.test(rule)) return 'windows'
    if (/linux/i.test(rule)) return 'linux'
    return "not fund"
}

export default getOS;