interface ISystem {
    [propName: string]: any
}
/**
 * 获取浏览器类型和版本
 * @param _window 含有navigator.userAgent的对象，默认是当前window 
 * @return {*} {browser: 浏览器名称, version: 浏览器版本}
 */
function getExplore(_window?: {
    navigator: {
        userAgent: string
    }
} | Window): {
    browser: | "IE"
    | "EDGE"
    | "Firefox"
    | "Chrome"
    | "Opera"
    | "Safari"
    | "not fund",
    version: string | number
} {
    let sys: ISystem = {}
    let ua = _window ? _window.navigator.userAgent.toLowerCase() : window.navigator.userAgent.toLowerCase()

    let s: RegExpMatchArray | null
        ; (s = ua.match(/rv:([\d.]+)\) like gecko/))
            ? (sys.ie = s[1])
            : (s = ua.match(/msie ([\d\.]+)/))
                ? (sys.ie = s[1])
                : (s = ua.match(/edge\/([\d\.]+)/))
                    ? (sys.edge = s[1])
                    : (s = ua.match(/firefox\/([\d\.]+)/))
                        ? (sys.firefox = s[1])
                        : (s = ua.match(/(?:opera|opr).([\d\.]+)/))
                            ? (sys.opera = s[1])
                            : (s = ua.match(/chrome\/([\d\.]+)/))
                                ? (sys.chrome = s[1])
                                : (s = ua.match(/version\/([\d\.]+).*safari/))
                                    ? (sys.safari = s[1])
                                    : 0
    // 根据关系进行判断
    if (sys.ie) return { browser: "IE", version: sys.ie }
    if (sys.edge) return { browser: "EDGE", version: sys.edge }
    if (sys.firefox) return { browser: "Firefox", version: sys.firefox }
    if (sys.chrome) return { browser: "Chrome", version: sys.chrome }
    if (sys.opera) return { browser: "Opera", version: sys.opera }
    if (sys.safari) return { browser: "Safari", version: sys.safari }
    return { browser: "not fund", version: "not fund" }
}

export default getExplore
