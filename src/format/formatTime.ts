/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 计算时间间隔
 * @FilePath: /burries/src/format/formatTime.ts
 */


import isNumber from "../regexp/isNumber"
/**
/**
 * 计算时间间隔，格式化时间为天、小时、分、秒的对象
 * @param {number} date 时间间隔，时间戳 ms
 * @return {*} 格式化后的对象
 */
function formatTime(date: number | string): { day: number, hour: number, min: number, seconds: number } {
    if (!date || !isNumber(date)) {
        return {
            day: 0,
            hour: 0,
            min: 0,
            seconds: 0,
        }
    }
    date = parseInt(date + '')
    var day = parseInt(date / (1000 * 60 * 60 * 24) + '');

    var leaveDay = date % (1000 * 60 * 60 * 24);

    var hour = parseInt(leaveDay / (1000 * 60 * 60) + '');

    var leaveHour = leaveDay % (1000 * 60 * 60);
    var min = parseInt(leaveHour / (1000 * 60) + '');

    var leaveMin = leaveHour % (1000 * 60);
    var seconds = leaveMin / 1000;



    return {
        day,
        hour,
        min,
        seconds,
    };
}

export default formatTime