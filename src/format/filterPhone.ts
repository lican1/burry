/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:37
 * @Description: 
 * @FilePath: /burries/src/format/filterPhone.ts
 */
import isPhoneNum from "../regexp/isPhoneNum"
/**
 * 电话号码中间四位做掩码，会忽略首尾空格
 * @param {*} value 源字符串
 * @return {*} 经过处理的字符串
 */
function filterPhone(value: number | string): string {
  if (!isPhoneNum(value)) {
    console.log('电话号码格式不正确');
    return ''
  }
  const val = String(value).trim()
  return val.slice(0, 3) + '****' + val.slice(7)
}

export default filterPhone





















