/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:37
 * @Description: 给数字中间每 3 位加逗号
 * @FilePath: /burries/src/format/commodify.ts
 */
import isNumber from "../regexp/isNumber"
/**
 * 给数字中间每 3 位加逗号
 * @param {number | string | bigint} n 源字符串
 * @return {*} 处理过的字符串
 */
function commodify(n: number | string | bigint): string {
  if (!isNumber(n)) {
    console.log('代入数据不合法');
    return ''
  }
  let re = /\d{1,3}(?=(\d{3})+$)/g;
  let n1 = n.toString().trim().replace(/^(\d+)((\.\d+)?)$/, function (s, s1, s2) {
    return s1.replace(re, '$&,') + s2;
  });
  return n1;
}
export default commodify






