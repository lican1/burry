/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 格式化日期
 * @FilePath: /burries/src/format/formatDate.ts
 */

import dayjs from 'dayjs'
import isNumber from "../regexp/isNumber"
import getTypeof from "../object//getTypeof"
/**
 * 格式化日期
 * @param {Date} param 代入的日期：支持时间字符串2021-12-20、时间戳、时间戳字符串、时间对象
 * @param {string} format 格式化范式 如： YYYY-DD-MM HH:mm:ss
 * @return {*}格式化后的字符串
 */
function formatDate(param: Date | number | string, format: string): string {
  let result: Date | number | string
  if (!param) {
    console.log('未能识别的参数');
    return ''
  }
  result = param
  // 是数字字符串当时间戳处理
  if (getTypeof(param) === 'string' && isNumber(<string>param)) {
    result = Number((<string>param).trim())
  }

  return dayjs(result).format(format)
}
export default formatDate