/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:37
 * @Description: 身份证中间生日日期做掩码
 * @FilePath: /burries/src/format/filterCard.ts
 */
import isIdCard from "../regexp/isIdCard"

/**
 *身份证中间生日日期做4位掩码，会忽略首尾空格
 * @param {number} value 源字符串
 * @return {*} 经过处理的字符串
 */
function filterCard(value: number | string | bigint): string {
  if (!isIdCard(value)) {
    console.log('身份证号格式不正确');
    return ''
  }
  // 删除收尾空格
  const val = String(value).trim()
  return val.slice(0, 10) + '****' + val.slice(14)
}

export default filterCard

