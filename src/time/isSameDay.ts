/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 
 * @FilePath: /burries/src/time/isSameDay.ts
 */
/**
 *   判断是否为同一天
 * @param  {Date} date1 需要比较的其中一个日期
 * @param  {Date} date2 可选／默认值：当天
 * @return {Boolean} 是否是同一天
 */
function isSameDay(date1: Date, date2?: Date): boolean {
    if (!date2) {
        date2 = new Date();
    }
    var date1_year = date1.getFullYear(),
        date1_month = date1.getMonth() + 1,
        date1_date = date1.getDate();
    var date2_year = date2.getFullYear(),
        date2_month = date2.getMonth() + 1,
        date2_date = date2.getDate()

    return date1_date === date2_date && date1_month === date2_month && date1_year === date2_year;

}

export default isSameDay
