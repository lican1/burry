/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 
 * @FilePath: /burries/src/time/monthDays.ts
 */
/** 
 *  获取指定日期月份的总天数
 * @param {Date} time 时间对象
 * @return {Number} 当前时间对象所在月份的天数
*/
function monthDays(time: Date): number {
    time = new Date(time);
    var year = time.getFullYear();
    var month = time.getMonth() + 1;
    return new Date(year, month, 0).getDate();
}
export default monthDays