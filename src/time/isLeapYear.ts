/*
 * @Author: 李灿
 * @Date: 2021-11-30 12:01:38
 * @Description: 是否为闰年
 * @FilePath: /burries/src/time/isLeapYear.ts
 */
/**
 * 
 *  是否为闰年
 * @param {Number} year 要判断的年份
 * @returns {Boolean} 是否是闰年
 */
/**
 * 是否为闰年 
 * @param {number} year 代入的年份
 * @return {*} 判断的结果
 */
function isLeapYear(year: number): boolean {
  if (0 === year % 4 && (year % 100 !== 0 || year % 400 === 0)) {
    return true
  }
  return false;
}

export default isLeapYear
