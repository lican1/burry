/*
 * @Author: 李灿
 * @Date: 2021-12-22 10:39:23
 * @Description: 节流函数, 高频触发事件，固定频率执行一次
 * @FilePath: /burries/src/function/throttle.ts
 */

/**
 * 节流函数, 高频触发事件，固定频率执行一次
 * @param {Function} fn 需要做节流处理的函数
 * @param {number} delay 时间间隔,可选参数，默认300 ms
 * @return {*} 经过接口处理的函数
 */
function throttle(fn: Function, delay: number = 300): Function {
  let btn = true
  return function (this: any, ...argument: any) {
    if (!btn) {
      return
    }
    btn = false
    fn.apply(this, argument)
    setTimeout(() => {
      btn = true
    }, delay)
  }

}

export default throttle
