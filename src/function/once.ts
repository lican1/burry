/*
 * @Author: 李灿
 * @Date: 2021-12-22 10:39:23
 * @Description: 无论调用多少次，只执行一次的函数
 * @FilePath: /burries/src/function/once.ts
 */

/**
 * 无论调用多少次，只执行一次的函数
 * @param {Function} handler 需要做只执行一次的函数处理
 * @return {*} 返回新的函数
 */
function once(handler: Function): Function {
  let btn = true;
  return function (this: any, ...arg: any[]) {
    if (btn) {
      handler.apply(this, ...arg);
      btn = false;
    }
  };
}
export default once