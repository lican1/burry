/*
 * @Author: 李灿
 * @Date: 2021-12-22 10:39:23
 * @Description: 防抖函数, 高频触发事件，只在短暂延时后执行最后一次。
 * @FilePath: /burries/src/function/debounce.ts
 */

/**
 *防抖函数, 高频触发事件，只在短暂延时后执行最后一次。
 * @param {Function} fn 需要做防抖处理的函数
 * @param {number} delay 延时时间,可选参数，默认300 ms
 * @return {*} 经过节流处理的函数
 */
function debounce(fn: Function, delay: number = 300): Function {
  let id: NodeJS.Timeout

  return function (this: any, ...arg: any[]) {
    clearTimeout(id)
    id = setTimeout(() => {
      fn.apply(this, arg)
    }, delay)
  }
}

export default debounce
