/*
 * @Author: 李灿
 * @Date: 2021-12-22 10:31:12
 * @Description: H5软键盘缩回、弹起回调
 * @FilePath: /burries/src/dom/windowResize.ts
 */
/**
 * 
 * H5软键盘缩回、弹起回调
 * 当软件键盘弹起会改变当前 window.innerHeight，监听这个值变化
 * @param {Function} downCb 当软键盘弹起后，缩回的回调
 * @param {Function} upCb 当软键盘弹起的回调
 */

function windowResize(downCb: Function, upCb: Function): void {
	var clientHeight = window.innerHeight;
	window.addEventListener('resize', () => {
		var height = window.innerHeight;
		if (height >= clientHeight) {
			downCb();
		}
		if (height < clientHeight) {
			upCb();
		}
	});
}

export default windowResize;