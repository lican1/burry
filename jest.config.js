/*
 * @Author: 李灿
 * @Date: 2021-11-30 14:41:32
 * @Description:
 */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  transformIgnorePatterns: ["/node_modules"],
  coverageThreshold: {
    //测试覆盖率, 阈值不满足，就返回测试失败
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90,
    },
  },
}
