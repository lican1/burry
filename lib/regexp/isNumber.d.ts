/**
 * 是否是纯数字字符串，不包含小数点,可包含负号
 * @param {*} value 需要做校验的字符串,会忽略左右空格
 * @return {*} 校验结果
 */
declare function isNumber(value: string | number | bigint): boolean;
export default isNumber;
