/**
 *
 *  判断是否为手机号
 * @param  {String|Number} str 需要做校验的字符串，会忽略前后空格
 * @return {Boolean} 是否是手机号
 */
declare function isPhoneNum(str: number | string): boolean;
export default isPhoneNum;
