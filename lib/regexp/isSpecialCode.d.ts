/**
 * 是否含有特殊字符
 * @param {*} value
 * @return {*}
 */
declare function isSpecialCode(value: string): boolean;
export default isSpecialCode;
