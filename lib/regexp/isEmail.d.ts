/**
 *
 *  判断是否为邮箱地址
 * @param  {String}  str 导入的字符串，会忽略前后的空格
 * @return {Boolean} 是否是邮箱
 */
declare function isEmail(str: string): boolean;
export default isEmail;
