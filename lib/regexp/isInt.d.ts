/**
 * 是否是正整数
 * @param {*} value 做校验的数字或者字符串，或忽略前后空格
 * @return {*} 判断结果
 */
declare function isInt(value: number | string | bigint): boolean;
export default isInt;
