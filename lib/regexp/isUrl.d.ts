/**
 *
 * 判断是否为URL地址, 会忽略前后空格
 * @param  {String} str
 * @return {Boolean}
 */
declare function isUrl(str: string): boolean;
export default isUrl;
