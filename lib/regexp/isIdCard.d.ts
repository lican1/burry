/**
 *
 *  判断是否为身份证号
 * @param  {String|Number} string 需要做判断的字符串或者数字，会忽略前后空格
 * @return {Boolean} 是否是身份证号
 */
declare function isIdCard(string: string | number | bigint): boolean;
export default isIdCard;
