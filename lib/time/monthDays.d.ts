/**
 *  获取指定日期月份的总天数
 * @param {Date} time 时间对象
 * @return {Number} 当前时间对象所在月份的天数
*/
declare function monthDays(time: Date): number;
export default monthDays;
