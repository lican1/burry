/**
 *   判断是否为同一天
 * @param  {Date} date1 需要比较的其中一个日期
 * @param  {Date} date2 可选／默认值：当天
 * @return {Boolean} 是否是同一天
 */
declare function isSameDay(date1: Date, date2?: Date): boolean;
export default isSameDay;
