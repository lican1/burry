/**
 *
 *  是否为闰年
 * @param {Number} year 要判断的年份
 * @returns {Boolean} 是否是闰年
 */
/**
 * 是否为闰年
 * @param {number} year 代入的年份
 * @return {*} 判断的结果
 */
declare function isLeapYear(year: number): boolean;
export default isLeapYear;
