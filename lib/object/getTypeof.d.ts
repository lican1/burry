/**
 * 获取对象的类型
 * @param {any} param 需要判断类型的对象
 * @return {*}
 */
declare function getTypeof(param: any): string;
export default getTypeof;
