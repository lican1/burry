/**
 * 获取路径上的参数
 * @param {string} param 要获取的属性
 * @param {string} search 查询的字符串，可选。默认为 location.search
 * @return {*} 查询的参数值
 */
declare function getRequest(param: string, search?: string): string;
export default getRequest;
