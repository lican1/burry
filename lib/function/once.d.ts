/**
 * 无论调用多少次，只执行一次的函数
 * @param {Function} handler 需要做只执行一次的函数处理
 * @return {*} 返回新的函数
 */
declare function once(handler: Function): Function;
export default once;
