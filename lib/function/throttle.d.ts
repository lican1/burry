/**
 * 节流函数, 高频触发事件，固定频率执行一次
 * @param {Function} fn 需要做节流处理的函数
 * @param {number} delay 时间间隔,可选参数，默认300 ms
 * @return {*} 经过接口处理的函数
 */
declare function throttle(fn: Function, delay?: number): Function;
export default throttle;
