/**
 *防抖函数, 高频触发事件，只在短暂延时后执行最后一次。
 * @param {Function} fn 需要做防抖处理的函数
 * @param {number} delay 延时时间,可选参数，默认300 ms
 * @return {*} 经过节流处理的函数
 */
declare function debounce(fn: Function, delay?: number): Function;
export default debounce;
