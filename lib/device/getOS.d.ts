/**
 * 获取操作系统类型
 * @param _window 含有navigator.userAgent的对象，默认是当前window
 * @return {*} 当前操作系统类型
 */
declare function getOS(_window?: {
    navigator: {
        userAgent: string;
    };
} | Window): "ios" | "android" | "windowsPhone" | "MacOSX" | "windows" | "linux" | "not fund";
export default getOS;
