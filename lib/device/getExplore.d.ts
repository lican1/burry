/**
 * 获取浏览器类型和版本
 * @param _window 含有navigator.userAgent的对象，默认是当前window
 * @return {*} {browser: 浏览器名称, version: 浏览器版本}
 */
declare function getExplore(_window?: {
    navigator: {
        userAgent: string;
    };
} | Window): {
    browser: "IE" | "EDGE" | "Firefox" | "Chrome" | "Opera" | "Safari" | "not fund";
    version: string | number;
};
export default getExplore;
