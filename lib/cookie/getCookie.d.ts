/**
 * 根据name读取cookie
 * @param {string} cookieName 要获取的 cookie 名
 * @return {string} 获取的结果
 */
declare function getCookie(cookieName: string): string;
export default getCookie;
