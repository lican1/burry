/**
 * 根据name去删除 cookie
 * @param {string} name 要移除的 cookie 名称
 */
declare function removeCookie(name: string): void;
export default removeCookie;
