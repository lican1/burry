/**
 * 根据name去设置 cookie
 * @param {string} name 设置的 cookie 名称(key)
 * @param {string} value 设置 cookie的值(value)
 * @param {number} days 设置的天数
 */
declare function setCookie(name: string, value: string, days?: number): void;
export default setCookie;
