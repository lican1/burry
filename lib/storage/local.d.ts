/**
 * localStorage 存取方法
 * @param {*} key 存入对象的key
 * @param {*} val 存入对象的 Value，可选，存在时代表存，不存在时代表读取。代入空字符串时代表清空
 * @return {*}
 */
declare function local(key: string, val?: any): any;
export default local;
