/**
 为元素移除 class
 *
 * @param {HTMLElement} ele 要移除 class 的目标元素
 * @param {string} cls 要移除的 class名称
 */
export default function removeClass(ele: HTMLElement, cls: string): void;
