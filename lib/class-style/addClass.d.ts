/**
 为元素添加class
 *
 * @param {HTMLElement} ele 要添加 class 的目标元素
 * @param {string} cls class 名称
 */
declare function addClass(ele: HTMLElement, cls: string): void;
export default addClass;
