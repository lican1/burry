/**
 *
 * 随机生成颜色
 * @return {String}
 */
declare function randomColor(): string;
export default randomColor;
