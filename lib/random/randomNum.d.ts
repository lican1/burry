/**
 *
 *  生成指定范围[min, max]的随机整数
 * @param  {Number} min 最小值
 * @param  {Number} max 最大值
 * @return {Number}
 */
declare function randomNum(min: number, max: number): number;
export default randomNum;
