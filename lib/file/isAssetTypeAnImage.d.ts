/**
 * 判断文件类型是不是图片
 * @param {File} file 文件对象
 * @return {*} true 表示是图片  false 表示不是图片
 */
declare function isAssetTypeAnImage(file: File): boolean;
export default isAssetTypeAnImage;
