/**
 * 判断文件大小是不是超出范围
 * @param {*} file 判断文件大小是不是超出范围
 * @param {*} maxSize 最大值 单位 M
 * @return {*} Boolean true 表示合法的文件大小
 */
declare function validateSize(file: File, maxSize: number): boolean;
export default validateSize;
