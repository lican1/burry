/**
 *  比较两个数组是否相等
 * @param  arr1 参与比较的数组
 * @param  arr2 参与比较的数组
 * @return true 代表相等，false 代表不相等
 */
declare function arrayEqual(arr1: Array<any>, arr2: Array<any>): boolean;
export default arrayEqual;
