/**
 *身份证中间生日日期做4位掩码，会忽略首尾空格
 * @param {number} value 源字符串
 * @return {*} 经过处理的字符串
 */
declare function filterCard(value: number | string | bigint): string;
export default filterCard;
