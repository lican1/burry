/**
/**
 * 计算时间间隔，格式化时间为天、小时、分、秒的对象
 * @param {number} date 时间间隔，时间戳 ms
 * @return {*} 格式化后的对象
 */
declare function formatTime(date: number | string): {
    day: number;
    hour: number;
    min: number;
    seconds: number;
};
export default formatTime;
