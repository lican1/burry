/**
 * 格式化日期
 * @param {Date} param 代入的日期：支持时间字符串2021-12-20、时间戳、时间戳字符串、时间对象
 * @param {string} format 格式化范式 如： YYYY-DD-MM HH:mm:ss
 * @return {*}格式化后的字符串
 */
declare function formatDate(param: Date | number | string, format: string): string;
export default formatDate;
