/**
 *
 * 现金额转大写
 * @param  {Number} n 代入的现金金额，会忽略数字前面的负号
 * @return {String} 转化后的结果
 */
declare function digitUppercase(n: number): string;
export default digitUppercase;
