/**
 * 给数字中间每 3 位加逗号
 * @param {number | string | bigint} n 源字符串
 * @return {*} 处理过的字符串
 */
declare function commodify(n: number | string | bigint): string;
export default commodify;
