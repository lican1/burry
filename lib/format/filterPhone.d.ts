/**
 * 电话号码中间四位做掩码，会忽略首尾空格
 * @param {*} value 源字符串
 * @return {*} 经过处理的字符串
 */
declare function filterPhone(value: number | string): string;
export default filterPhone;
